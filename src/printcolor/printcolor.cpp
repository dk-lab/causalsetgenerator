/////////////////////////////
//(C) Will Cunningham 2017 //
//         DK Lab          //
// Northeastern University //
/////////////////////////////

#include "printcolor.h"

//Calls to these functions will change the default
//color of text printed to the terminal

//Cyan
void printf_cyan()
{
	printf("\x1b[36m");
}

//Red
void printf_red()
{
	printf("\x1b[31m");
}

//Yellow
void printf_yel()
{
	printf("\x1b[33m");
}

//Magenta
void printf_mag()
{
	printf("\x1b[35m");
}

//Return to no color (default)
void printf_std()
{
	printf("\x1b[0m");
}
